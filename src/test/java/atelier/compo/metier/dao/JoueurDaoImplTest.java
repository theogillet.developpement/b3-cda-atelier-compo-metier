package atelier.compo.metier.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import atelier.compo.metier.joueur.Joueur;
import static org.junit.jupiter.api.Assertions.*;

class JoueurDaoImplTest {

    private JoueurDaoImpl joueurDao;
    private ArrayList<Joueur> listJoueurs;

    @BeforeEach
    void setUp() {
        listJoueurs = new ArrayList<>();
        joueurDao = new JoueurDaoImpl(listJoueurs);
    }

    @Test
    void testAddJoueur() {
        Joueur joueur = new Joueur("John", 1000);
        Joueur joueur2 = new Joueur("skyart", 1000);
        Joueur joueur3 = new Joueur("chelxie", 1100);
        joueurDao.addJoueur(joueur);
        joueurDao.addJoueur(joueur2);
        joueurDao.addJoueur(joueur3);
        assertEquals(3, joueurDao.getTousLesJoueurs().size());
        assertEquals("John", joueurDao.getTousLesJoueurs().get(0).getNom());
        assertEquals("skyart", joueurDao.getTousLesJoueurs().get(1).getNom());
        assertEquals("chelxie", joueurDao.getTousLesJoueurs().get(2).getNom());
    }

    @Test
    void testGetTousLesJoueurs() {
        Joueur joueur = new Joueur("John", 1000);
        Joueur joueur2 = new Joueur("skyart", 1000);
        Joueur joueur3 = new Joueur("chelxie", 1100);
        joueurDao.addJoueur(joueur);
        joueurDao.addJoueur(joueur2);
        joueurDao.addJoueur(joueur3);
        assertEquals(3, joueurDao.getTousLesJoueurs().size());
        assertEquals("John", joueurDao.getTousLesJoueurs().get(0).getNom());
        assertEquals("skyart", joueurDao.getTousLesJoueurs().get(1).getNom());
        assertEquals("chelxie", joueurDao.getTousLesJoueurs().get(2).getNom());
    }

    @Test
    void testUpdateJoueur() {
        Joueur joueur = new Joueur("John", 1000);
        joueurDao.addJoueur(joueur);
        Joueur updatedJoueur = new Joueur("John Updated", 1500);
        joueurDao.updateJoueur(updatedJoueur, 0);
        Joueur retrievedJoueur = joueurDao.getTousLesJoueurs().get(0);
        assertEquals("John Updated", retrievedJoueur.getNom());
        assertEquals(1500, retrievedJoueur.getCash());
    }

    @Test
    void testDeleteJoueur() {
        Joueur joueur1 = new Joueur("John", 1000);
        Joueur joueur2 = new Joueur("Jane", 2000);
        joueurDao.addJoueur(joueur1);
        joueurDao.addJoueur(joueur2);
        joueurDao.deleteJoueur(joueur1);
        ArrayList<Joueur> joueurs = joueurDao.getTousLesJoueurs();
        assertEquals(1, joueurs.size());
        assertEquals("Jane", joueurs.get(0).getNom());
    }

    @Test
    void testAjoutCashJoueurs(){
        Joueur joueur1 = new Joueur("John", 1000);
        Joueur joueur2 = new Joueur("Jane", 1000);
        joueurDao.addJoueur(joueur1);
        joueurDao.addJoueur(joueur2);
        ArrayList<Joueur> joueurs = joueurDao.getTousLesJoueurs();
        joueurs.stream().map(joueur -> {
            joueur.setCash(joueur.getCash() + 100);
            return joueur;
        }).forEach(joueur -> joueurDao.updateJoueur(joueur, joueurs.indexOf(joueur)));;

        ArrayList<Joueur> updatedJoueurs = joueurDao.getTousLesJoueurs();
        for (Joueur joueur : updatedJoueurs) {
            assertEquals(1100, joueur.getCash());
        }
    }
    @Test
    void afficherListJoueurs(){
        Joueur joueur1 = new Joueur("John", 1000);
        Joueur joueur2 = new Joueur("Jane", 1000);
        joueurDao.addJoueur(joueur1);
        joueurDao.addJoueur(joueur2);
        ArrayList<Joueur> joueurs = joueurDao.getTousLesJoueurs();
        joueurs.stream()
                .map(joueur -> "Nom: " + joueur.getNom() + ", Cash: " + joueur.getCash())
                .forEach(System.out::println);
    }
}
