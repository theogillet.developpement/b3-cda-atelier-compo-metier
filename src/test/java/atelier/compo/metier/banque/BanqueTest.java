package atelier.compo.metier.banque;

import atelier.compo.metier.banque.Banque;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BanqueTest {

    @AfterEach
    void tearDown() {

        Banque.resetInstance();
    }
    @ParameterizedTest
    @CsvSource({
            "0, 0",
            "1000, 1000",
            "500,500"
    })
    void createBanqueGetCash(int initialCash, int expectedCash) {
        Banque banque = Banque.getInstance(initialCash);

        assertEquals(expectedCash, banque.getCash());
    }
}
