package atelier.compo.metier.fabrique;

import atelier.compo.metier.propriete.Propriete;
import atelier.compo.metier.type.TypePropriete;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FabriqueProprieteTest {

    @ParameterizedTest
    @CsvSource({
            "terrain, Rue de la Paix, 400",
            "terrain, Rue de Courcelles, 100",
            "gare, Gare Montparnasse, 200",
    })
    void creerPropriete(String typePropriete, String nom, int prix) {
        FabriquePropriete fabriquePropriete = new FabriquePropriete();

        Propriete propriete = fabriquePropriete.creer(TypePropriete.fromString(typePropriete), nom, prix);

        assertEquals(propriete.getNom(), nom);
        assertEquals(propriete.getPrix(), prix);
    }
}
