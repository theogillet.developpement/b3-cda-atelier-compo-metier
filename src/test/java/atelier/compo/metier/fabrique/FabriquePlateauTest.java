package atelier.compo.metier.fabrique;

import atelier.compo.metier.plateau.Case;
import atelier.compo.metier.plateau.Plateau;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FabriquePlateauTest {

    @Test
    void creerPlateau() {
        FabriquePlateau fabriquePlateau = new FabriquePlateau();
        FabriqueCase fabriqueCase = new FabriqueCase();

        List<Case> listeDeCases = new ArrayList<>();
        listeDeCases.add(fabriqueCase.creer(1, "Case 1"));
        listeDeCases.add(fabriqueCase.creer(2, "Case 2"));
        listeDeCases.add(fabriqueCase.creer(3, "Case 3"));
        listeDeCases.add(fabriqueCase.creer(4, "Case 4"));
        listeDeCases.add(fabriqueCase.creer(5, "Case 5"));
        listeDeCases.add(fabriqueCase.creer(6, "Case 6"));
        listeDeCases.add(fabriqueCase.creer(7, "Case 7"));
        listeDeCases.add(fabriqueCase.creer(8, "Case 8"));
        listeDeCases.add(fabriqueCase.creer(9, "Case 9"));
        listeDeCases.add(fabriqueCase.creer(10, "Case 10"));

        Plateau plateau = fabriquePlateau.creer(listeDeCases);

        assertEquals(10, plateau.nbCases());
        assertEquals("Case 6", plateau.getCase(5).getNom());
    }

    @Test
    void boucleCasesDuPlateau () {
        FabriquePlateau fabriquePlateau = new FabriquePlateau();
        FabriqueCase fabriqueCase = new FabriqueCase();

        List<Case> listeDeCases = new ArrayList<>();
        listeDeCases.add(fabriqueCase.creer(1, "Case 1"));
        listeDeCases.add(fabriqueCase.creer(2, "Case 2"));
        listeDeCases.add(fabriqueCase.creer(3, "Case 3"));
        listeDeCases.add(fabriqueCase.creer(4, "Case 4"));
        listeDeCases.add(fabriqueCase.creer(5, "Case 5"));
        listeDeCases.add(fabriqueCase.creer(6, "Case 6"));
        listeDeCases.add(fabriqueCase.creer(7, "Case 7"));
        listeDeCases.add(fabriqueCase.creer(8, "Case 8"));
        listeDeCases.add(fabriqueCase.creer(9, "Case 9"));
        listeDeCases.add(fabriqueCase.creer(10, "Case 10"));

        Plateau plateau = fabriquePlateau.creer(listeDeCases);

        for (int i = 0; i < plateau.nbCases(); i++) {
            System.out.println("Numéro de case : " + plateau.getCase(i).getNumero() + " & Nom de case : " + plateau.getCase(i).getNom());
        }
    }

}
