CREATE TABLE joueur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    cash INT
);

INSERT INTO joueur (nom, cash) VALUES ('John', 1000);
INSERT INTO joueur (nom, cash) VALUES ('Jane', 1500);
INSERT INTO joueur (nom, cash) VALUES ('Alice', 2000);