package atelier.compo.metier.iterator;

import atelier.compo.metier.plateau.Case;

import java.util.ArrayList;

public class PlateauIterator implements Iterator<Case> {

    private int index;
    private ArrayList<Case> listeDeCases;

    public PlateauIterator(ArrayList<Case> listeDeCases) {
        this.listeDeCases = listeDeCases;
    }

    @Override
    public boolean hasNext() {
        return null == this.listeDeCases.get(this.index + 1);
    }

    @Override
    public Case next() {
        return listeDeCases.get(index + 1);
    }
}
