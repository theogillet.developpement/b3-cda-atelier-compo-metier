package atelier.compo.metier.iterator;

public interface Iterator<T> {

    boolean hasNext();

    T next();
}
