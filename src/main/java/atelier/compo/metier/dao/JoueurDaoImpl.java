package atelier.compo.metier.dao;

import atelier.compo.metier.joueur.Joueur;

import java.util.ArrayList;

public class JoueurDaoImpl implements JoueurDao{

    private ArrayList<Joueur> listJoueurs;

    public JoueurDaoImpl(ArrayList<Joueur> listJoueurs) {
        this.listJoueurs = listJoueurs;
    }

    @Override
    public ArrayList<Joueur> getTousLesJoueurs() {
        return listJoueurs;
    }

    @Override
    public void addJoueur(Joueur joueur) {
        listJoueurs.add(joueur);
    }

    @Override
    public Joueur updateJoueur(Joueur joueur, int i) {
        if (i >= 0 && i < listJoueurs.size()) {
            Joueur existingJoueur = listJoueurs.get(i);
            if (joueur.getNom() != null) {
                existingJoueur.setNom(joueur.getNom());
            }
            existingJoueur.setCash(joueur.getCash());
            return existingJoueur;
        }
        return null;
    }

    @Override
    public void deleteJoueur(Joueur joueur) {
        listJoueurs.remove(joueur);
    }



}
