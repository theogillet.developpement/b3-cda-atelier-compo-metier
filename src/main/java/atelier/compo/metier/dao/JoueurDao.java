package atelier.compo.metier.dao;

import atelier.compo.metier.joueur.Joueur;

import java.util.ArrayList;

public interface JoueurDao {
//    La base de données pourra être simulée :
//            - la méthode getTousLesJoueurs renverra une liste en mémoire
//- les méthodes addJoueur, updateJoueur, deleteJoueur travailleront sur cette liste

      ArrayList<Joueur> getTousLesJoueurs();
      Joueur updateJoueur(Joueur joueur, int i);
      void deleteJoueur(Joueur joueur);
      void addJoueur(Joueur joueur);



}
