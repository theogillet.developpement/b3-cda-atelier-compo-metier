package atelier.compo.metier.plateau;

public class Case {

    private int numero;

    public Case(int numero, String nom) {
        this.numero = numero;
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    private String nom;


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
