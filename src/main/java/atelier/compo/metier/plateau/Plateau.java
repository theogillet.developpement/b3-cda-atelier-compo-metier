package atelier.compo.metier.plateau;

import atelier.compo.metier.iterator.PlateauIterator;

import java.util.ArrayList;
import java.util.List;

public class Plateau implements Aggregate {
    private List<Case> cases = new ArrayList<>();

    public Plateau(List<Case> cases) {
        this.cases = cases;
    }

    public void ajouterCase(Case c) { cases.add(c); }

    public Case getCase(int i) {
        return cases.get(i);
    }

    public int nbCases() {
        return cases.size();
    }

    public List<Case> getCases() {
        return cases;
    }
}
