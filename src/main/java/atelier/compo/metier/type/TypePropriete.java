package atelier.compo.metier.type;

public enum TypePropriete {
    TERRAIN("terrain"),
    GARE("gare"),
    COMPAGNIE("compagnie");

    private final String type;

    TypePropriete(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

    public static TypePropriete fromString(String text) {
        for (TypePropriete type : TypePropriete.values()) {
            if (type.type.equalsIgnoreCase(text)) {
                return type;
            }
        }
        throw new IllegalArgumentException("No enum constant with text " + text);
    }
}
