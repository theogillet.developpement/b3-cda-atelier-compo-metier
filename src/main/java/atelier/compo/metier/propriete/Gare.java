package atelier.compo.metier.propriete;

public class Gare implements Propriete {
    private int prix;
    private String nom;

    public Gare(String nom, int prix) {
        this.nom = nom;
        this.prix = prix;
    }

    @Override
    public int getPrix() {
        return this.prix;
    }

    @Override
    public String getNom() {
        return this.nom;
    }
}
