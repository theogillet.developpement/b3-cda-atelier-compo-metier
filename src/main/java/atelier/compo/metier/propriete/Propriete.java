package atelier.compo.metier.propriete;

public interface Propriete {
    int getPrix();
    String getNom();

}
