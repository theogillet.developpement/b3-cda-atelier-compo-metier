package atelier.compo.metier.joueur;

public class JoueurControleur {

    public JoueurControleur() {
    }

    public String getNom(Joueur joueur) {
        return joueur.getNom();
    }

    public int getCash(Joueur joueur) {
        return joueur.getCash();
    }
    public void setNom(Joueur joueur, String nom) {
        joueur.setNom(nom);
    }

    public void setCash(Joueur joueur, int cash) {
        joueur.setCash(cash);
    }

    public void updateVue(Joueur joueur) {
        new JoueurVue().afficherFicheJoueur(joueur.getNom(), joueur.getCash());
    }

}
