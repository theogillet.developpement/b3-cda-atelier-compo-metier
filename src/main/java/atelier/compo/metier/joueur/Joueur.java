package atelier.compo.metier.joueur;

public class Joueur {

    private String nom;
    private int cash;

    public Joueur(String nom, int cash) {
        this.nom = nom;
        this.cash = cash;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }
}
