package atelier.compo.metier.banque;

import atelier.compo.metier.joueur.Joueur;

public final class Banque extends Joueur {
    private static Banque instance = null;

    public Banque(int cash) {
        super("banque", cash);
    }

    public static Banque getInstance(int initialCash) {
        if (instance == null) {
            instance = new Banque(initialCash);
        }
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }
}