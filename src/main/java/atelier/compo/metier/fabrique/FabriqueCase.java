package atelier.compo.metier.fabrique;

import atelier.compo.metier.plateau.Case;

public class FabriqueCase implements Fabrique {

    public Case creer(int numero, String nom) {
        return new Case(numero, nom);
    }

}
