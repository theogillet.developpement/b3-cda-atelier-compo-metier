package atelier.compo.metier.fabrique;

import atelier.compo.metier.propriete.Compagnie;
import atelier.compo.metier.propriete.Gare;
import atelier.compo.metier.propriete.Propriete;
import atelier.compo.metier.propriete.Terrain;
import atelier.compo.metier.type.TypePropriete;

public class FabriquePropriete implements Fabrique {

    public Propriete creer(TypePropriete typePropriete, String nom, int prix) {
        return switch (typePropriete) {
            case TypePropriete.TERRAIN -> new Terrain(nom, prix);
            case TypePropriete.GARE -> new Gare(nom, prix);
            case TypePropriete.COMPAGNIE -> new Compagnie(nom, prix);
        };
    }

}
