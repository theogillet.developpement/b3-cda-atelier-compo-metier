package atelier.compo.metier.fabrique;

import atelier.compo.metier.plateau.Case;
import atelier.compo.metier.plateau.Plateau;

import java.util.Collections;
import java.util.List;

public class FabriquePlateau {

    public Plateau creer(List<Case> cases) {
        return new Plateau(cases);
    }

}
