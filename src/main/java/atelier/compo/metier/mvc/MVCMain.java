package atelier.compo.metier.mvc;

import atelier.compo.metier.MetierApplication;
import atelier.compo.metier.joueur.Joueur;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MVCMain {

    public static void main(String[] args) {
        SpringApplication.run(MetierApplication.class, args);
    }





}
